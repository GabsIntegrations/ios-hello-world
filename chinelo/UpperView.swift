//
//  UpperView.swift
//  chinelo
//
//  Created by Gabriel Aguido Fraga on 06/07/2018.
//  Copyright © 2018 Gabriel Aguido Fraga. All rights reserved.
//

import UIKit

class UpperView: UIViewController {

    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var swit: UISwitch!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var labelNumber: UILabel!
    var onOff: Bool!
    var num: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        hideAll()
        
    }
    
    func hideAll() {
        segment.isHidden = true
        textField.isHidden = true
        slider.isHidden = true
        swit.isHidden = true
        stepper.isHidden = true
        loading.isHidden = true
        labelNumber.isHidden = true
        onOff = false
        num = 0
        progress.progress = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == UISwipeGestureRecognizerDirection.up {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstView") as? FirstView
            
            mvc?.modalTransitionStyle = .flipHorizontal
            
            self.present(mvc!, animated: true, completion: nil)
            
        } else {
            return
        }
    }
    
    @IBAction func listenToAtivar(_ btn: UIButton) {
        print(progress.progress)
        
        onOff = !onOff
        if onOff {
            btn.setTitle("Desativar", for: .normal)
            segment.isHidden = false
            segment.selectedSegmentIndex = -1
            progress.setProgress(10, animated: true)
        } else {
            btn.setTitle("Ativar", for: .normal)
            hideAll()
            textField.text = "Digite slider"
            slider.value = slider.value/2

        }
    }
    
    @IBAction func segmentedValueChanged(_ seg: UISegmentedControl) {
        
        if seg.selectedSegmentIndex == 0 {
            textField.isHidden = false
        } else {
            textField.isHidden = true
        }
    }
    
   
    @IBAction func valueChanged(_ txtFld: UITextField) {
        
        if txtFld.text == "slider".lowercased() {
            slider.isHidden = false
        } else {
            slider.isHidden = true
        }
    }
    
    @IBAction func listenToSlider(_ sld: UISlider) {
        
        if sld.maximumValue == sld.value {
            swit.isHidden = false
            swit.setOn(false, animated: false)
            loading.isHidden = false
            loading.startAnimating()
        } else {
            swit.isHidden = true
            loading.isHidden = true
        }
    }
    
    @IBAction func switchChanged(_ swt: UISwitch) {
        
        if swt.isOn {
            loading.isHidden = true
            stepper.isHidden = false
            labelNumber.isHidden = false
        } else {
            loading.isHidden = false
            stepper.isHidden = true
            stepper.isHidden = true
        }
    }
    
    @IBAction func changeValue(_ stp: UIStepper) {
        labelNumber.text = String(stp.value)
        
        if stp.value == 10 && progress.progress == 1.0 {
            
            let alert = UIAlertController(title: "Vencedor", message: "Parabens, você chegou ao final", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    
}
