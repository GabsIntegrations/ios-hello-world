//
//  ViewController.swift
//  chinelo
//
//  Created by Gabriel Aguido Fraga on 05/07/2018.
//  Copyright © 2018 Gabriel Aguido Fraga. All rights reserved.
//

import UIKit

class FirstView: UIViewController {

    @IBOutlet var label: UILabel!
    @IBOutlet var swit: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == UISwipeGestureRecognizerDirection.left && self.swit.isOn {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RightTableView") as? RightTableView
            
            mvc?.modalTransitionStyle = .crossDissolve
            
            self.present(mvc!, animated: true, completion: nil)
            
        } else if gesture.direction == UISwipeGestureRecognizerDirection.right && self.swit.isOn {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftCollectionView") as? LeftCollectionView
            
            mvc?.modalTransitionStyle = .crossDissolve
            
                self.present(mvc!, animated: true, completion: nil)
            
        } else if gesture.direction == UISwipeGestureRecognizerDirection.up && self.swit.isOn {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LowerView") as? LowerView
            
            mvc?.modalTransitionStyle = .crossDissolve
            
            self.present(mvc!, animated: true, completion: nil)
            
        } else if gesture.direction == UISwipeGestureRecognizerDirection.down && self.swit.isOn {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpperView") as? UpperView
            
            mvc?.modalTransitionStyle = .crossDissolve
            
            self.present(mvc!, animated: true, completion: nil)
        } else {
            return
        }

    }
    
    @IBAction func listenToSwitch(_ swit: UISwitch) {
        
        var estado: String
        estado = ""
        
        if swit.isOn {
            estado = "ativada"
            self.label.text = "Navegação ativada"
        } else {
            estado = "desativada"
            self.label.text = "Navegação desativada"
        }
        
        let alert = UIAlertController(title: "Ativação da navegação", message: "Navegação \(estado)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
        
    }

}

