//
//  LowerView.swift
//  chinelo
//
//  Created by Gabriel Aguido Fraga on 06/07/2018.
//  Copyright © 2018 Gabriel Aguido Fraga. All rights reserved.
//

import UIKit

class LowerView: UIViewController {

    @IBOutlet var myWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://www.itau.com.br");
        let requestObj = URLRequest(url: url!);
        myWebView.loadRequest(requestObj);

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == UISwipeGestureRecognizerDirection.down {
            
            let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstView") as? FirstView
            
            mvc?.modalTransitionStyle = .flipHorizontal
            
            self.present(mvc!, animated: true, completion: nil)
            
        } else {
            return
        }
        
    }
    
    
}
